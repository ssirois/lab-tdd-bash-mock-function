# How to mock a bash function

At one point in a project, I needed to mock a bash function declared in
the script under test. After a research on the Web, didn't find what I
was searching for, therefore this lab project to explore a solution.

Don't get me wrong. I found beautiful stuff out there. This [Alex
Harvey](https://alex-harvey-z3q.github.io/) person has awesome articles on
techniques to unit test bash scripts. You'll find techniques to mock
commands, spies and what not. If testing bash script is an interesting
topic for you, that blog will really interest you.

Those two articles in particular helped my thinking on the topic of a
mock for a bash function:

* [Unit Testing a Bash Script with
shUnit2](https://alex-harvey-z3q.github.io/2017/07/07/unit-testing-a-bash-script-with-shunit2.html)
* [Testing AWS CLI scripts in
shUnit2](https://alex-harvey-z3q.github.io/2018/09/07/testing-aws-cli-scripts-in-shunit2.html)

## Explored solution in this lab project

In short, to get a mock on a function defined in the script under test
you need:

* to define the mock
    * needs to be named exactly as the real function;
    * needs to be exported;
* to define the function only if a function with the same name is not
already defined

Two helper functions grown out of this lab project:

* [isNotDefined](https://gitlab.com/ssirois/lab-tdd-bash-mock-function/-/blob/master/src/a-script#L6)
* [mockFunction](https://gitlab.com/ssirois/lab-tdd-bash-mock-function/-/blob/master/tests/mock)
