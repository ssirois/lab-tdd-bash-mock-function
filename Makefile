rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
TEST_FILES:=$(call rwildcard,./tests/,test-*)
TEST_TARGETS=$(patsubst %,check_%,$(TEST_FILES))

.PHONY: check
check: $(TEST_TARGETS)

check_%:
	$(*)

check_./%:
	$(*)

